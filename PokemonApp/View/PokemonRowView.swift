//
//  PokemonRowView.swift
//  PokemonApp
//
//  Created by Tavo Lopez on 1/07/21.
//  Copyright © 2021 Tavo Lopez. All rights reserved.
//

import SwiftUI
import KingfisherSwiftUI

struct PokemonRowView: View {
    
    let pokemon: PokemonModel
    
    init(pokemon: PokemonModel) {
        self.pokemon = pokemon
    }
    
    var body: some View {
        
        VStack {
            ZStack{
                BodyView(pokemon: self.pokemon)
                ImageView(pokemon: self.pokemon)
            }
        }
        .frame(width: UIScreen.main.bounds.width - 70, height: 220)
        .background(Color.white)
        .cornerRadius(10)
        .shadow(color: Color.black.opacity(0.2), radius: 5, x: 5, y: 5)
    }
}

struct BodyView: View {
    
    var pokemon: PokemonModel
    
    var body: some View {
        VStack {
            
            VStack {
                Text("")
            }
            .frame(width: UIScreen.main.bounds.width - 70, height: 100)
            .background(self.pokemon.color)
            
            
            VStack {
                Text("")
            }
            .frame(width: UIScreen.main.bounds.width - 70, height: 120)
            
        }
        .frame(width: UIScreen.main.bounds.width - 70)
    }
}

struct ImageView: View {
    
    var pokemon: PokemonModel
    
    var body: some View {
        VStack {
            
            ZStack {
                
                Color.white
                    .clipShape(Circle())
                    .frame(width: 130, height: 130)
                    .padding(.all, 4)
                    .background(Color.gray.opacity(0.6))
                    .clipShape(Circle())
                
                KFImage(URL(string: self.pokemon.avatar))
                    .resizable()
                    .scaledToFit()
                    .frame(width: 120, height: 120)
            }
            
            Text(self.pokemon.name.capitalized)
                .font(.system(size: 25, design: .rounded))
                .foregroundColor(self.pokemon.color.opacity(0.7))
                .bold()
            
        }
    }
}

struct PokemonRowView_Previews: PreviewProvider {
    static var previews: some View {
        PokemonRowView(pokemon: Constants.POKEMON_DEFAULT)
    }
}
