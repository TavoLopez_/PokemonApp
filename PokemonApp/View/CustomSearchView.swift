//
//  CustomSearchView.swift
//  PokemonApp
//
//  Created by Tavo Lopez on 5/07/21.
//  Copyright © 2021 Tavo Lopez. All rights reserved.
//

import SwiftUI

struct CustomSearchView : UIViewRepresentable {

    @Binding var txt: String

    func makeCoordinator() -> Coordinator {

        return CustomSearchView.Coordinator(parent: self)

    }

    func makeUIView(context: Context) -> UISearchBar {

        let view = UISearchBar(frame: .zero)
        view.autocorrectionType = .no
        view.autocapitalizationType = .none
        view.searchBarStyle = .minimal
        view.searchTextField.placeholder = "Buscar un Pokemon"
        view.delegate = context.coordinator

        return view
    }

    func updateUIView(_ uiView:  UISearchBar, context: Context) { }
    class Coordinator : NSObject,UISearchBarDelegate{

        var managed: CustomSearchView

        init(parent: CustomSearchView) {

            managed = parent
        }

        func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

            self.managed.txt = searchText

        }
    }
}
