//
//  ActivityIndicatorView.swift
//  PokemonApp
//
//  Created by Tavo Lopez on 5/07/21.
//  Copyright © 2021 Tavo Lopez. All rights reserved.
//

import SwiftUI

import SwiftUI

struct ActivityIndicatorView: UIViewRepresentable {
    
    let style: UIActivityIndicatorView.Style
    let color: String
    
    init(style: UIActivityIndicatorView.Style, color: String) {
        self.style = style
        self.color = color
    }
    
    func makeUIView(context: UIViewRepresentableContext<ActivityIndicatorView>) -> UIActivityIndicatorView {
        
        let indicator = UIActivityIndicatorView()
        indicator.style = self.style
        if !self.color.isEmpty {
            indicator.color = UIColor(named: color)
        }
        indicator.startAnimating()
        return indicator
    
    }
    
    func updateUIView(_ uiView: UIActivityIndicatorView, context: UIViewRepresentableContext<ActivityIndicatorView>) {
    }
}
