//
//  FavoritesView.swift
//  PokemonApp
//
//  Created by Tavo Lopez on 3/07/21.
//  Copyright © 2021 Tavo Lopez. All rights reserved.
//

import SwiftUI

struct FavoritesView: View {
    
    @FetchRequest(fetchRequest: Pokemon.getPokemons()) var favoritesPokemon: FetchedResults<Pokemon>
    @Environment(\.presentationMode) var closeSheet
    @State var pokemonSelected = Constants.POKEMON_DEFAULT
    @State var navigateViewPokemonDetail = false
    @State var pokemonList = [PokemonModel]()
    @State  var namePokemon = ""
    var vmPokemon: PokemonViewModel
    let gfService = GlobalFunctions()
    
    var body: some View {
        
        ZStack {
            
            NavigationLink(destination: PokemonDetailView(pokemon: self.pokemonSelected, vmPokemon: vmPokemon), isActive: self.$navigateViewPokemonDetail) {
                Text("")
            }
            
            VStack(spacing: 20) {
                
                CustomSearchView(txt: self.$namePokemon)
                
                List(self.pokemonList.filter {
                    self.namePokemon.isEmpty ? true : $0.name.lowercased().contains(self.namePokemon.lowercased())
                }, id: \.self) { i  in
                    PokemonRowView(pokemon: i)
                        .onTapGesture {
                            self.pokemonSelected = i
                            self.navigateViewPokemonDetail = true
                    }
                }
            }
            .navigationBarTitle("\(Constants.NAVIGATION_TITLE_FAVORITES)", displayMode: .large)
            .navigationBarItems(trailing: NavigationFavoritesTrailingView(amount: self.pokemonList.count))
            .padding()
        }
        .onAppear {
            if self.favoritesPokemon.count > 0 {
                self.setPokemons()
            } else {
                self.closeSheet.wrappedValue.dismiss()
            }
        }
    }
    
    private func setPokemons() {
        if self.pokemonList.count > 0 {
            self.pokemonList.removeAll()
        }
        for item in self.favoritesPokemon {
            let pokemon = PokemonModel(id: item.id,
                                       name: item.name,
                                       type: item.type,
                                       weight: item.weight,
                                       height: item.height,
                                       abilities: self.gfService.stringToArray(string: item.abilities),
                                       moves: self.gfService.stringToArray(string: item.moves),
                                       experience: item.experience,
                                       avatar: item.avatar,
                                       color: self.gfService.getBackgroundColor(type: item.type))
            self.pokemonList.append(pokemon)
        }
    }
}

struct NavigationFavoritesTrailingView: View {
    var amount = 0
    var body: some View {
        Text("\(amount)")
            .font(.system(.body, design: .rounded))
            .bold()
            .foregroundColor(Color.red)
            .padding(10)
            .background(Color.red.opacity(0.3))
            .clipShape(Circle())
    }
}

struct FavoritesView_Previews: PreviewProvider {
    static var previews: some View {
        FavoritesView(vmPokemon: PokemonViewModel())
    }
}
