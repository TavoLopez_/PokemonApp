//
//  PokemonDetailView.swift
//  PokemonApp
//
//  Created by Tavo Lopez on 3/07/21.
//  Copyright © 2021 Tavo Lopez. All rights reserved.
//

import SwiftUI
import KingfisherSwiftUI

struct PokemonDetailView: View {

    @Environment(\.managedObjectContext) var context
    @Environment(\.presentationMode) var closeSheet
    @State var isFavorite = false
    var pokemon: PokemonModel
    var vmPokemon: PokemonViewModel
    
    var body: some View {
        
        ZStack(alignment: Alignment(horizontal: .center, vertical: .top)) {
            
            VStack {
                
                Spacer()

                VStack {
                    
                    KFImage(URL(string: self.pokemon.avatar))
                        .resizable()
                        .scaledToFit()
                        .frame(width: 220, height: 220)

                    ZStack {
                        Text(self.pokemon.name.capitalized)
                            .font(.system(size: 30, design: .rounded))
                            .foregroundColor(Color.white)
                            .bold()
                        
                        
                        HStack {

                            Spacer()
                            
                            Button(action: {
                                
                                self.addOrDeleteFavoritePokemon()
                                
                            }){
                                
                                Image(systemName: (self.isFavorite) ? Constants.DISLIKE : Constants.LIKE)
                                    .font(.system(.title, design: .rounded))
                                    .foregroundColor(Color.white)
                                    .padding()
                                    .background(Blureffect())
                                    .clipShape(Circle())
                                    .shadow(color: Color.black.opacity(0.2), radius: 6)
                                
                            }
                            
                        }
                    }
                }
                .padding(.bottom)
                
                VStack {
                    
                    BodyDetailView(pokemon: self.pokemon)
                    
                }
                .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height / 2)
                .background(Color.white)
                .clipShape(CustomCorner(corners: [.topLeft, .topRight], size: 30))
            }
            .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            .background(self.pokemon.color)
            
        }
        .edgesIgnoringSafeArea(.all)
        .onAppear{
            let result = self.vmPokemon.fetchFavoritePokemon(id: self.pokemon.id, context: self.context)
            if result != nil {
                self.isFavorite = true
            }
        }
    }
    
    private func addOrDeleteFavoritePokemon() {
        if self.isFavorite {
            let result = self.vmPokemon.fetchFavoritePokemon(id: self.pokemon.id, context: self.context)
            if result != nil {
                let result = self.vmPokemon.deleteFavorite(pokemon: result!, context: self.context)
                if result {
                    self.closeSheet.wrappedValue.dismiss()
                }
            }
        } else {
            self.isFavorite = self.vmPokemon.addFavorite(pokemon: self.pokemon, context: self.context)
        }
    }
}

struct BodyDetailView: View {
    
    var pokemon: PokemonModel
    
    var body: some View {
        
        VStack(spacing: 15) {
            
            HStack(spacing: 25){

                VStack {
                    
                    DatesView(value: self.pokemon.weight, title: Constants.WEIGHT, size: 30, sizeTitle: 13, color: self.pokemon.color)
                    DatesView(value: self.pokemon.height, title: Constants.HEIGHT, size: 30, sizeTitle: 13, color: self.pokemon.color)
                    
                }
                
                DatesView(value: self.pokemon.experience, title: Constants.EXPERIENCE, size: 80, sizeTitle: 20, color: self.pokemon.color)

            }
            
            VStack(spacing: 15) {

                if self.pokemon.abilities.count > 0 {
                    ArrayView(title: Constants.HABILITIES, data: self.pokemon.abilities, color: self.pokemon.color)
                }
                
                if self.pokemon.moves.count > 0 {
                    ArrayView(title: Constants.MOVES, data: self.pokemon.moves, color: self.pokemon.color)
                }
            }
        }
        .padding(.bottom, 25)
    }
}

struct DatesView: View {
    
    var value: Int
    var title: String
    var size: CGFloat
    var sizeTitle: CGFloat
    var color: Color
    
    var body: some View {
        VStack {
            
            Text("\(value)")
                .font(.system(size: size, weight: .bold, design: .rounded))
            
            Text(title)
                .font(.system(size: sizeTitle, weight: .bold, design: .rounded))
                .foregroundColor(color.opacity(0.5))
        }
    }
}

struct ArrayView: View {
    
    var title: String
    var data: [String]
    var color: Color

    var body: some View {

        VStack(spacing: 15) {

            Text(self.title)
                .font(.system(size: 25, weight: .bold, design: .rounded))
                
            HStack {
                ForEach(0..<self.data.count) { (i) in
                    
                    Text(self.data[i])
                        .font(.subheadline)
                        .foregroundColor(self.color)
                        .bold()
                        .padding(.horizontal, 16)
                        .padding(.vertical, 8)
                        .overlay(
                            RoundedRectangle(cornerRadius: 18)
                                .fill(self.color.opacity(0.20))
                        )
                        .frame(height: 25)
                }
            }
        }
        .frame(width: UIScreen.main.bounds.width - 50, height: 100)
    }
}

struct PokemonDetailView_Previews: PreviewProvider {
    static var previews: some View {
        PokemonDetailView(isFavorite: true, pokemon: Constants.POKEMON_DEFAULT, vmPokemon: PokemonViewModel())
    }
}
