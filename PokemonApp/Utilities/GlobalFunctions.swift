//
//  GlobalFunctions.swift
//  PokemonApp
//
//  Created by Tavo Lopez on 1/07/21.
//  Copyright © 2021 Tavo Lopez. All rights reserved.
//

import Foundation
import SwiftyJSON
import SwiftUI

class GlobalFunctions: NSObject {
    
    public func getURL(endpoint: String, offset: Int, limit: Int) -> URL {
        let url = endpoint + "?offset=\(offset)&limit=\(limit)"
        return URL(string: url)!
    }
    
    public func getURL(endpoint: String) -> URL {
        return URL(string: endpoint)!
    }
    
    public func getBackgroundColor(type: String) -> Color {
        switch type {
        case "normal": return Color.orange
        case "fighting": return Color.red
        case "flying": return Color.blue
        case "poison": return Color.green
        case "ground": return Color.gray
        case "rock": return Color.yellow
        case "bug": return Color.green
        case "ghost": return Color.orange
        case "steel": return Color.green
        case "fire": return Color.red
        case "water": return Color.blue
        case "grass": return Color.yellow
        case "electric": return Color.yellow
        case "psychic": return Color.gray
        case "ice": return Color.blue
        case "dragon": return Color.red
        case "dark": return Color.black
        case "fairy": return Color.purple
        case "unknown": return Color.pink
        case "shadow": return Color.primary
        default: return Color.pink
        }
    }
    
    public func stringToArray(string: String) -> [String] {
        if string.count > 0 {
            return string.components(separatedBy: ",")
        } else {
            return [string]
        }
    }
    
    public func arrayToString(array: [String]) -> String {
        var result = ""
        for item in array {
            result += item + ","
        }
        result = String(result.dropLast())
        return result
    }
    
    public func genericAlert(title: String, message: String) -> Void {
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Aceptar", style: .default) { (action: UIAlertAction) in }
        alertVC.addAction(okAction)
        
        let viewController = UIApplication.shared.windows.first!.rootViewController!
        viewController.present(alertVC, animated: true, completion: nil)
    }
    
    public func extractArrayOfJson(jsonList: JSON, value: String) -> [String] {
        var result: [String] = []
        for item in jsonList {
            let json = item.1[value]
            let element = json["name"].string ?? ""
            result.append(element)
            if value == "move" {
                if result.count == 5 {
                    break
                }
            }
        }
        return result
    }
    
}
