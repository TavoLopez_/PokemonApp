//
//  Enumerations.swift
//  PokemonApp
//
//  Created by Tavo Lopez on 2/07/21.
//  Copyright © 2021 Tavo Lopez. All rights reserved.
//

import Foundation
enum TypeAction {
    case next
    case back
}
enum ContextActions {
    case save
    case delete
}
