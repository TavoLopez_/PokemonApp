//
//  Constants.swift
//  PokemonApp
//
//  Created by Tavo Lopez on 1/07/21.
//  Copyright © 2021 Tavo Lopez. All rights reserved.
//

import Foundation
import SwiftUI
import CoreData

struct Constants {
    // MARK: - API
    static let BASEURL = "https://pokeapi.co/api/v2/pokemon/"
    static let POKEMON_DEFAULT = PokemonModel(id: 0, name: "names", type: "fire", weight: 20, height: 30, abilities: [], moves: [], experience: 50, avatar: "", color: Color.green)
    // MARK: - Titles
    static let NEXT = "Siguiente"
    static let BACK = "Anterior"
    static let NAVIGATION_TITLE_HOME = "Pokemon App"
    static let NAVIGATION_TITLE_FAVORITES = "Mis Favoritos"
    static let WARNING = "Advertencia"
    static let MOVES = "Moves"
    static let HABILITIES = "Habilities"
    static let EXPERIENCE = "Experience"
    static let HEIGHT = "HEIGHT"
    static let WEIGHT = "WEIGHT"
    static let GET_POKEMONS = "Obteniendo Pokemones"
    // MARK: - Icons
    static let CHEVRON_LEFT = "chevron.left"
    static let CHEVRON_RIGHT = "chevron.right"
    static let LIKE = "hand.thumbsup"
    static let DISLIKE = "hand.thumbsup.fill"
}
