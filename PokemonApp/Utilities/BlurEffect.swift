//
//  BlurEffect.swift
//  PokemonApp
//
//  Created by Tavo Lopez on 2/07/21.
//  Copyright © 2021 Tavo Lopez. All rights reserved.
//

import SwiftUI

struct Blureffect : UIViewRepresentable {
    
    func makeUIView(context: UIViewRepresentableContext<Blureffect>) -> UIVisualEffectView {
        let view = UIVisualEffectView(effect: UIBlurEffect(style: .systemUltraThinMaterial))
        return view
    }
    
    func updateUIView(_ uiView: UIVisualEffectView, context: UIViewRepresentableContext<Blureffect>) {}
}
