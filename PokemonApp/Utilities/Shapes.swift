//
//  Shapes.swift
//  PokemonApp
//
//  Created by Tavo Lopez on 3/07/21.
//  Copyright © 2021 Tavo Lopez. All rights reserved.
//

import SwiftUI

struct CustomCorner: Shape {
    
    var corners: UIRectCorner
    var size: Int
    
    func path(in rect: CGRect) -> Path {
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: corners, cornerRadii: CGSize(width: size, height: size))
        return Path(path.cgPath)
    }
}
