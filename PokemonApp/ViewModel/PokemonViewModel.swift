//
//  PokemonViewModel.swift
//  PokemonApp
//
//  Created by Tavo Lopez on 1/07/21.
//  Copyright © 2021 Tavo Lopez. All rights reserved.
//

import Foundation
import SwiftyJSON
import CoreData

class PokemonViewModel: ObservableObject {
    
    private static let sessionProcessingQueue = DispatchQueue(label: "SessionProcessingQueue")
    private var url = Constants.BASEURL
    private let gfService = GlobalFunctions()
    public var dataUrlPokemons = [Result]()

    @Published var offset = 0
    @Published var limit = 3
    @Published var listPokemon = [PokemonModel]()

    init() {
        fetchPokemons()
    }
    
    func fetchPokemons() {
        getDataUrlPokemons { (_) in
            for item in self.dataUrlPokemons {
                self.getDataPokemon(endpoint: item.url)
            }
        }
    }
    
    func getDataUrlPokemons(completion: @escaping(Bool) -> Void) {
        let url = gfService.getURL(endpoint: self.url, offset: self.offset, limit: self.limit)
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else { return }
            let json = JSON(data)
            let jsonResults = json["results"]

            for item in jsonResults {
                let element = Result(name: item.1["name"].string ?? "", url: item.1["url"].string ?? "")
                self.dataUrlPokemons.append(element)
            }
            completion(true)
        }.resume()
    }

    func getDataPokemon(endpoint: String) {
        
        let url = gfService.getURL(endpoint: endpoint, offset: self.offset, limit: self.limit)
        
        let _ = URLSession.shared.dataTaskPublisher(for: url)
            .subscribe(on: Self.sessionProcessingQueue)
            .map({
                let data = $0.data
                let json = JSON(data)
                return self.extractPokemon(json: json)
            })
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { (suscriberCompletion) in
                switch suscriberCompletion {
                case .finished:
                    break
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }, receiveValue: { [weak self] (pokemon) in
                self?.listPokemon.append(pokemon)
            })
    }
    
    func extractPokemon(json: JSON) -> PokemonModel {
        let id = json["id"].int ?? 0
        let name = json["name"].string ?? ""
        let jsonTypes = json["types"]
        let type = jsonTypes[0]["type"]["name"].string ?? ""
        let weight = json["weight"].int ?? 0
        let height = json["height"].int ?? 0
        let jsonAbilities = json["abilities"]
        let jsonMoves = json["moves"]
        let experience = json["base_experience"].int ?? 0
        let jsonSprites = json["sprites"]
        let avatar = jsonSprites["front_default"].string ?? ""
        let abilities = self.gfService.extractArrayOfJson(jsonList: jsonAbilities, value: "ability")
        let moves = self.gfService.extractArrayOfJson(jsonList: jsonMoves, value: "move")
        let color = self.gfService.getBackgroundColor(type: type)
        let pokemon = PokemonModel(id: id, name: name, type: type, weight: weight, height: height, abilities: abilities, moves: moves, experience: experience, avatar: avatar, color: color)
        return pokemon
    }
    
    public func fetchFavoritePokemon(id: Int, context: NSManagedObjectContext) -> Pokemon? {
        let fetchRequest: NSFetchRequest<Pokemon> = Pokemon.getPokemons()
        fetchRequest.predicate = NSPredicate(format: "%K == %@", argumentArray: ["id", id])
        return try? context.fetch(fetchRequest).first
    }

    public func changePage(typeAction: TypeAction) {
        switch typeAction {
        case .next:
            self.offset += self.listPokemon.count
        case .back:
            self.offset -= self.limit
        }
        self.dataUrlPokemons.removeAll()
        self.listPokemon.removeAll()
        self.fetchPokemons()
    }
    
    public func addFavorite(pokemon: PokemonModel, context: NSManagedObjectContext) -> Bool {
        let model = Pokemon(context: context)
        model.id = pokemon.id
        model.name = pokemon.name
        model.type = pokemon.type
        model.weight = pokemon.weight
        model.height = pokemon.height
        model.abilities = self.gfService.arrayToString(array: pokemon.abilities)
        model.moves = self.gfService.arrayToString(array: pokemon.moves)
        model.experience = pokemon.experience
        model.avatar = pokemon.avatar
        return self.saveContext(context: context)
    }
      
    public func deleteFavorite(pokemon: Pokemon, context: NSManagedObjectContext) -> Bool {
        context.delete(pokemon)
        return self.saveContext(context: context)
    }
    
    public func saveContext(context: NSManagedObjectContext) -> Bool {
        do {
            try context.save()
            return true
        } catch let error as NSError {
            self.gfService.genericAlert(title: Constants.WARNING, message: error.localizedDescription)
            return false
        }
    }
}

extension Data {
    func removeNullOfData(removeString string: String) -> Data? {
        let dataString = String(data: self, encoding: .utf8)
        let parseDataString = dataString?.replacingOccurrences(of: string, with: ",")
        guard let data = parseDataString?.data(using: .utf8) else { return nil }
        return data
    }
}
