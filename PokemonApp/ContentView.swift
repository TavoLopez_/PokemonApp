//
//  ContentView.swift
//  PokemonApp
//
//  Created by Tavo Lopez on 1/07/21.
//  Copyright © 2021 Tavo Lopez. All rights reserved.
//
import SwiftUI

struct ContentView: View {
    
    @FetchRequest(fetchRequest: Pokemon.getPokemons()) var favoritesPokemon: FetchedResults<Pokemon>
    @ObservedObject var vmPokemon = PokemonViewModel()
    @State var pokemonSelected = Constants.POKEMON_DEFAULT
    @State var navigateViewPokemonDetail = false

    init() {
        UITableView.appearance().separatorStyle = .none
    }

    var body: some View {
        
        NavigationView {
            
            ZStack {
                
                NavigationLink(destination: PokemonDetailView(pokemon: self.pokemonSelected, vmPokemon: self.vmPokemon),
                               isActive: self.$navigateViewPokemonDetail) {
                    Text("")
                }
                .navigationBarTitle("\(Constants.NAVIGATION_TITLE_HOME)", displayMode: .large)
                .navigationBarItems(leading: NavigationViewLeading(countFavorites: self.favoritesPokemon.count, vmPokemon: self.vmPokemon))
                
                if self.vmPokemon.listPokemon.count > 0 {
                    
                    VStack {
                        List(self.vmPokemon.listPokemon, id: \.self) { i  in
                            PokemonRowView(pokemon: i)
                                .onTapGesture {
                                    self.pokemonSelected = i
                                    self.navigateViewPokemonDetail = true
                            }
                        }
                        .padding()
                    }
                    .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - 150, alignment: .center)
                    
                    VStack {
                        
                        Spacer()
                        
                        HStack {
                            
                            Button(action: {
                                self.vmPokemon.changePage(typeAction: .back)
                            }){

                                HStack {
                                    Image(systemName: Constants.CHEVRON_LEFT)
                                    Text(Constants.BACK)
                                }
                                .font(.system(size: 15, weight: .semibold, design: .rounded))
                                .foregroundColor(Color.red.opacity((self.vmPokemon.offset == 0) ? 0.3 : 1))
                            }
                            .disabled(self.vmPokemon.offset == 0)
                            
                            Spacer()
                            
                            Button(action: {
                                self.vmPokemon.changePage(typeAction: .next)
                            }){
                                HStack {
                                    Text(Constants.NEXT)
                                    Image(systemName: Constants.CHEVRON_RIGHT)
                                }
                                .font(.system(size: 15, weight: .semibold, design: .rounded))
                                .foregroundColor(Color.red)
                            }
                        }
                        .frame(width: UIScreen.main.bounds.width - 50)
                        .padding(20)
                        .background(Color.red.opacity(0.29))
                        .clipShape(Capsule())
                    }
                } else {
                    
                    VStack {
                        ActivityIndicatorView(style: .large, color: "red")
                        Text(Constants.GET_POKEMONS)
                            .font(.system(size: 17, weight: .bold, design: .rounded))
                            .foregroundColor(.gray)
                    }
                    
                }
            }
        }
        .accentColor((navigateViewPokemonDetail) ? Color.white : Color.red)
    }
}

struct NavigationViewLeading: View {
    
    var countFavorites = 0
    var vmPokemon: PokemonViewModel
    var body: some View {
        
        VStack {
            if countFavorites > 0 {
                    
                NavigationLink(destination: FavoritesView(vmPokemon: vmPokemon)) {
                    
                    Image(systemName: Constants.DISLIKE)
                        .font(.system(size: 20, weight: .semibold, design: .rounded))
                        .foregroundColor(Color.red)
                        .padding()
                }
            } else {
                Text("")
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
