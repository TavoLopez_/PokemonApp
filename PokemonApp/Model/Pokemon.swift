//
//  Pokemon.swift
//  PokemonApp
//
//  Created by Tavo Lopez on 3/07/21.
//  Copyright © 2021 Tavo Lopez. All rights reserved.
//

import Foundation
import CoreData

class Pokemon: NSManagedObject, Identifiable {
    @NSManaged public var id : Int
    @NSManaged public var name : String
    @NSManaged public var type : String
    @NSManaged public var weight : Int
    @NSManaged public var height : Int
    @NSManaged public var abilities : String
    @NSManaged public var moves : String
    @NSManaged public var experience : Int
    @NSManaged public var avatar : String
}

extension Pokemon {
    static func getPokemons() -> NSFetchRequest<Pokemon> {
        let request : NSFetchRequest<Pokemon> = Pokemon.fetchRequest() as! NSFetchRequest<Pokemon>
        request.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]
        return request
    }
}
