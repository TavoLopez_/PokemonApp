//
//  DataUrlPokemons.swift
//  PokemonApp
//
//  Created by Tavo Lopez on 1/07/21.
//  Copyright © 2021 Tavo Lopez. All rights reserved.
//

import Foundation

// MARK: - Result
struct Result: Codable {
    let name: String
    let url: String
}
