//
//  PokemonModel.swift
//  PokemonApp
//
//  Created by Tavo Lopez on 1/07/21.
//  Copyright © 2021 Tavo Lopez. All rights reserved.
//

import Foundation
import SwiftUI

struct PokemonModel: Identifiable, Hashable {
    let id : Int
    let name : String
    let type : String
    let weight : Int
    let height : Int
    let abilities : [String]
    let moves : [String]
    let experience : Int
    let avatar : String
    let color : Color
}
